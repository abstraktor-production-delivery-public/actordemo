# ActorDemo
ActorDemo is a demo app used in [ActorJs](https://www.npmjs.com/package/@abstraktor/actorjs) workshop. The workshop goes through some examples of how to use [ActorJs](https://www.npmjs.com/package/@abstraktor/actorjs).

## Prerequisites
[Nodejs](https://nodejs.org/en/) &#8211; 14.x.x or 16.x.x
[Git](https://git-scm.com/) &#8211; 2.26.0 or later

## Install ActorDemo

```bash
npm install @abstraktor/actordemo
```

## Update ActorJs
```bash
npm update
```
For nodejs 14.x.x you probably need to run:
```bash
npm install @abstraktor/actordemo@latest
```
**Linux & Mac:** The first time you install ActorDemo, you need to restart the terminal or ***source*** the *.bashrc* or *.profile* file.
```
source ~/.bashrc
```

## Build ActorDemo Server
```bash
ad debug
```

## Start ActorDemo Client
Open a browser with the url http://localhost:9045

## Beta Testers
We are looking for beta testers!
To apply, send an e-mail to: betatester@actorjs.com

## Documentation
* [https://actorjs.com/documentation](https://actorjs.com/documentation)

## Youtube
* [Abstraktor Videos](https://www.youtube.com/channel/UCBidy7zm5mez27BMfx4_nfA/featured)

## Further reading in the ActorJs tool
The ActorJs documentation can be found in the tool. To access it, click the following links when the tool is running.
* [Documentation](http://localhost:9005/documentation)
* [Education](http://localhost:9005/education)
* [Videos](http://localhost:9005/videos)

